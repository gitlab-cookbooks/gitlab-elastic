require_relative "spec_helper"

describe "gitlab-elastic::default" do
  let(:device) { "/dev/gitlab_vg/elastic_var" }
  let(:prometheus_exporter) do
    {
      install: true
    }
  end
  let(:fstab_settings) do
    {
      device_type: :device,
      fstype: "ext4",
      options: ["defaults", "noatime", "nobootwait"]
    }
  end
  let(:chef_run) do
    ChefSpec::SoloRunner.new { |node|
      node.set["gitlab-elastic"]["fstab"] = fstab_settings
      node.set["gitlab-elastic"]["fstab"]["device"] = device
      node.set["gitlab-elastic"]["fstab"]["options"] = fstab_settings[:options].join(",")
      node.set["gitlab-elastic"]["fstab"]["device_type"] = "device"
      node.set["gitlab-elastic"]["prometheus_exporter"] = prometheus_exporter
    }.converge(described_recipe)
  end

  it "install elastic search" do
    expect(chef_run).to install_package('elasticsearch')
  end

  it "install prometheus_exporter plugin" do
    expect(chef_run).to run_bash("install_prometheus_exporter_plugin")
  end

  context do
    let(:template_file) { "/etc/elasticsearch/jvm.options" }

    it "creates jvm.options configuration file" do
      expect(chef_run).to render_file( template_file )
    end

    it "restarts elasticsearch delayed" do
      resource = chef_run.template(template_file)
      expect(resource).to notify("service[elasticsearch]").to(:restart).delayed
    end
  end

  context do
    let(:template_file) { "/etc/elasticsearch/elasticsearch.yml" }

    it "creates elasticsearch configuration file" do
      expect(chef_run).to render_file(template_file)
    end

    it "restarts elasticsearch delayed" do
      resource = chef_run.template(template_file)
      expect(resource).to notify("service[elasticsearch]").to(:restart).delayed
    end
  end
end
